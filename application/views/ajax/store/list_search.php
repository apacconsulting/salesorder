<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th class="text-left">Mã NSX</th>
        <th class="text-left">Tên NSX</th>
        <th class="text-left">Số Điện Thoại</th>
        <th class="text-left">Email</th>
        <th class="text-left">Địa Chỉ</th>
        <th class="text-left">Ghi chú</th>
        <th style="width: 80px;"></th>
    </tr>
    </thead>
    <tbody>
    <?php if (isset($_list_prd_group)) :
        foreach ($_list_prd_group as $key => $item): ?>
            <tr class='tr-item-<?php echo $item['id']; ?>'>
                <td class="text-edit" onclick="cms_detail_store(<?php echo $item['ID']; ?>)"><?php echo $item['store_code']; ?></td>
                <td class="text-edit" onclick="cms_detail_store(<?php echo $item['ID']; ?>)"><?php echo $item['store_name']; ?></td>
                <td class="text-center"><?php echo $item['store_phone']; ?></td>
                <td class="text-center"><?php echo $item['store_mail']; ?></td>
                <td class="text-center"><?php echo $item['store_address']; ?></td>
                <td class="text-center"><?php echo $item['notes']; ?></td>
                <td class="text-center"></td>
            </tr>
           
        <?php endforeach;
    else: ?>
        <tr>
            <td colspan="7" class="text-center">Không có dữ liệu</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
<?php if (isset($_pagination_link) && !empty($_pagination_link)) { ?>
    <div class="alert alert-info summany-info clearfix" role="alert"
         style="background: #fff; margin-bottom: 0; border: none;">
        <div class="pull-right ajax-pagination">
            <?php echo $_pagination_link; ?>
        </div>
    </div>
<?php } ?>