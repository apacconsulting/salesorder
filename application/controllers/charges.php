<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Charges extends CI_Controller
{
	public $auth;
    private $messages = '0';

    public function __construct()
    {
        parent::__construct();
        $this->auth = $this->cms_authentication->check();
    }

    public function index($page = 1)
    {
    	if ($this->auth == null || !in_array(12, $this->auth['group_permission']))
            $this->cms_common_string->cms_redirect(CMS_BASE_URL . 'backend');

        $data['seo']['title'] = "Phần mềm quản lý bán hàng";
        $config = $this->cms_common->cms_pagination_custom();
        $data['_pagination_link'] = $this->pagination->create_links();
        $data['_list_customer'] = $this->db;
        $config['base_url'] = 'cms_paging_listcustomer';
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $data['user'] = $this->auth;

    	$data['template'] = 'charges/index';
    	$this->load->view('layout/index', isset($data) ? $data : null);
    }

    public function cms_create_address()
    {
        if ($this->auth == null)
            $this->cms_common_string->cms_redirect(CMS_BASE_URL . 'backend');

        $data = $this->input->post('data');
        //print_r($data['prd_address']);
        //$data['level'] = 0;
        if (isset($data['parentid']) && $data['parentid'] > 0) {
            //$level = $this->db->select('level')->from('address')->where('ID', $data['parentid'])->limit(1)->get()->row_array();
            //$data['level'] = $level['level'] + 1;
            $prd_group = $this->db->from('address')->where(['parentid' => $data['parentid'], 'address_name' => $data['data']['prd_address']])->get()->row_array();
        } else {
            $prd_group = $this->db->from('address')->where(['parentid' => 0, 'address_name' => $data['address_name']])->get()->row_array();
        }

        if (!empty($prd_group) && count($prd_group)) {
            echo $this->messages = '0';
            return;
        } else {
            
            $this->db->insert('address', $data);
            echo $this->messages = '1';
        }
    }

    public function cms_paging_group($page = 1)
    {
        $this->cms_nestedset->setAddress('address');
        $config = $this->cms_common->cms_pagination_custom();
        $total_prdGroup = $this->db->from('address')->count_all_results();
        $config['base_url'] = 'cms_paging_address';
        $config['total_rows'] = $total_prdGroup;
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $data['_pagination_link'] = $this->pagination->create_links();
        //$data ['_list_prd_group'] = $this->cms_nestedset->datacharges('address', NULL, ['per_page' => $config['per_page'], 'page' => $page]);
        $data ['_list_prd_group'] =  $this->cms_nestedset->datacharges('address', NULL, ['per_page' => $config['per_page'], 'page' => $page]);
        //if ($page > 1 && ($total_prdGroup - 1) / ($page - 1) == 10)
        //    $page = $page - 1;

        $data ['page'] = $page;
        $this->load->view('ajax/address/list_prd_group', isset($data) ? $data : null);
    }

    public function cms_save_item_address($id)
    {
        $id = (int)$id;
        $data = $this->input->post('data');
        $prd_group = $this->db->from('address')->where('id', $id)->get()->row_array();
        if (empty($prd_group) && count($prd_group) == 0) {
            echo $this->messages = '0';
            return;
        }
        //$prd_group_check = $this->db->from('address')->where(['parentid' => $prd_group['parentid'], 'address_name' => $data['address_name']])->get()->row_array();
        if (empty($prd_group_check) || count($prd_group_check) == 0) {
            $this->db->where('ID', $id)->update('address', $data);
            echo $this->messages = '1';
        } else
            echo $this->messages = '0';
    }

    public function cms_load_address()
    {
        //$this->cms_nestedset->set('address');
        //$sls_group = $this->cms_nestedset->dropdown('address', NULL, 'address');
        $sls_group = $this->db->from('address')->where('parentid >', 0)->get()->result_array();
        ob_start();
        echo '<option value="-1" selected="selected">--Địa Điểm--</option>';
        echo '<optgroup label="Chọn Địa Điểm">';
        if ($sls_group)
            foreach ($sls_group as $key => $val) :
                ?>
                <option
                    value="<?php echo $val['ID']; ?>"><?php echo $val['address_name']; ?>
                </option>
            <?php
            endforeach;
        echo '</optgroup>';
        $html = ob_get_contents();
        ob_end_clean();
        echo $this->messages = $html;
    }
}