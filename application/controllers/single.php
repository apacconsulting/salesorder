<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Single extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
       
    }

    public function index()
    {
    	$data['template'] = 'single/index';
        $this->load->view('layout/web', isset($data) ? $data : null);
    }

}