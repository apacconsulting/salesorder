<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Controller
{
    private $auth;

    public function __construct()
    {
        parent::__construct();
        $this->auth = $this->cms_authentication->check();
    }

    public function index($page = 1)
    {
    	if ($this->auth == null || !in_array(14, $this->auth['group_permission']))
            $this->cms_common_string->cms_redirect(CMS_BASE_URL . 'backend');

        $data['seo']['title'] = "Phần mềm quản lý bán hàng";
        $config = $this->cms_common->cms_pagination_custom();
        $data['user'] = $this->auth;
    	$data['template'] = 'general/index';
        $this->load->view('layout/index', isset($data) ? $data : null);
    }
}